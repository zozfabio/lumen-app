<?php

use App\Model\ModelColumn;

$modelPackage = "\App\Model";

$idColumn = [
    "name"       => "id",
    "type"       => ModelColumn::TYPE_INT,
    "fillable"   => false,
    "primaryKey" => true,
];
$simpleNameColumn = [
    "name" => "name",
    "type" => ModelColumn::TYPE_STRING,
    "size" => 100,
];

$controllerPackage = "\App\Http\Controllers";

$resourcePackage = "\App\Http\Resources";

return [
    [
        "model" => [
            "package" => $modelPackage,
            "name"    => "Country",

            "table"   => "country",

            "columns" => [
                $idColumn,
                $simpleNameColumn,
            ],
        ],
        "resource" => [
            "package" => $resourcePackage,
            "name"    => "CountryResource",
        ],
    ],
    [
        "model" => [
            "package" => $modelPackage,
            "name"    => "State",

            "table"   => "state",

            "columns" => [
                $idColumn,
                $simpleNameColumn,
            ],
        ],
        "resource" => [
            "package" => $resourcePackage,
            "name"    => "StateResource",
        ],
    ],
    [
        "model" => [
            "package" => $modelPackage,
            "name"    => "City",

            "table"   => "city",

            "columns" => [
                $idColumn,
                $simpleNameColumn,
            ],
        ],
        "resource" => [
            "package" => $resourcePackage,
            "name"    => "CityResource",
        ],
    ],
    [
        "model" => [
            "use" => \App\Model\User::class,
        ],
        "resource" => [
            "package" => $resourcePackage,
            "name"    => "UserResource",
        ],
        "resourceCollection" => [
            "package" => $resourcePackage,
            "name"    => "UserResourceCollection",
        ],
        "controller" => [
            "package" => $controllerPackage,
            "name"    => "UserController",

            "arguments" => [
                [
                    "name"   => "name_like",
                    "type"   => \App\Http\Arguments\ArgumentContains::class,
                    "column" => "name",
                ],
            ],
        ],
        "route" => [
            "name" => "user",
            "path" => "/user",
        ],
    ],
    [
        "model" => [
            "package" => $modelPackage,
            "name"    => "UserContact",

            "table"   => "user_contact",

            "columns" => [
                $idColumn,
                [
                    "name" => "description",
                    "type" => ModelColumn::TYPE_STRING,
                    "size" => 100,
                ],
                [
                    "name"    => "user",
                    "type"    => ModelColumn::TYPE_MANY_TO_ONE,
                    "related" => \App\Model\User::class
                ],
            ],
        ],
        "resource" => [
            "package" => $resourcePackage,
            "name"    => "UserContactResource",
        ],
        "resourceCollection" => [
            "package" => $resourcePackage,
            "name"    => "UserContactResourceCollection",
        ],
    ],
];
