<?php

namespace App\Console\Gen;

use App\Model\ModelColumn;
use Illuminate\Support\Str;

class GenModel {

    protected $namespace = "";

    protected $name = "";

    protected $table = "";

    /**
     * @var ModelColumn[]
     */
    protected $columns = [];

    /**
     * @param string $namespace
     * @param string $name
     */
    private function __construct($namespace, $name) {
        $this->namespace = $namespace;
        $this->name      = $name;
    }

    /**
     * @param string $namespace
     * @param string $name
     *
     * @return self
     */
    public static function create($namespace, $name) {
        GenPreconditions::nonEmpty($namespace, "Model namespace cannot be empty!");
        GenPreconditions::nonEmpty($name,      "Model name cannot be empty!");

        return new self($namespace, $name);
    }

    public function setTable(string $table): self {
        $this->table = $table;
        return $this;
    }

    public function addArgument(ModelColumn $column): self {
        $this->columns[] = $column;
        return $this;
    }

    public function setColumns(array $columns): self {
        $this->columns = [];
        foreach ($columns as $column) {
            $this->addArgument($column);
        }
        return $this;
    }

    private function renderTable(GenClass $class) {
        $class->addConstant("TABLE", "\"{$this->table}\"")
            ->addProperty("protected", "string", "table", "self::TABLE");
    }

    private function renderColumns(GenClass $class) {
        if (sizeof($this->columns) == 0) return;

        $class->addImport(ModelColumn::class);

        $value = "[".PHP_EOL;
        foreach ($this->columns as $column) {
            $relatedName = "";
            if ($column->isRelation()) {
                list(, $relatedName) = GenUtils::extractClassPackageAndName($column->getRelated());
                $class->addImport($column->getRelated());
            }

            $value .= "            ModelColumn::of(\"{$column->getName()}\", ";
            $value .= "ModelColumn::TYPE_{$column->getType()}, ";
            $value .= "{$column->getSize()}, ";
            $value .= ($column->isNullable()   ? "true" : "false").", ";
            $value .= ($column->isPrimaryKey() ? "true" : "false").", ";
            $value .= ($column->isFillable()   ? "true" : "false").", ";
            $value .= ($column->isHidden()     ? "true" : "false").", ";
            $value .= ($column->isRelation()   ? "{$relatedName}::class" : '""');
            $value .= "),".PHP_EOL;
        }
        $value .= "        ];".PHP_EOL;

        $class->addFixedValueGetter("public", "ModelColumn[]", "columns", $value);
    }

    /**
     * @return string
     */
    public function render() {
        $class = GenClass::create($this->namespace, $this->name)
            ->addImport("App\Model\Model")
            ->setExtend("Model");

        $this->renderTable($class);
        $this->renderColumns($class);

        return $class->render();
    }

    public function save() {
        $namespace = Str::startsWith($this->namespace, "\\") ?
            $this->namespace :
            "\\{$this->namespace}";

        $pathName = GenUtils::fromClassNameToPathName("{$namespace}\\{$this->name}");

        GenFile::create($pathName)
            ->contents($this->render())
            ->save();
    }
}
