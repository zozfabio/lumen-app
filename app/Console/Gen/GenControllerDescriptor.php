<?php

namespace App\Console\Gen;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerArgument;

class GenControllerDescriptor {

    /** @var string */
    private $use;

    /** @var string */
    private $package;

    /** @var string */
    private $name;

    /** @var string */
    protected $modelName;

    /** @var string */
    protected $modelClassName;

    /** @var string */
    protected $resourceName;

    /** @var string */
    protected $resourceClassName;

    /** @var string */
    protected $resourceCollectionName;

    /** @var string */
    protected $resourceCollectionClassName;

    /** @var ControllerArgument[] */
    private $arguments;

    private function __construct(string $use,
                                 string $package,
                                 string $name,
                                 string $modelName,
                                 string $modelClassName,
                                 string $resourceName,
                                 string $resourceClassName,
                                 string $resourceCollectionName,
                                 string $resourceCollectionClassName,
                                 array $arguments)
    {
        $this->use                         = $use;
        $this->package                     = $package;
        $this->name                        = $name;
        $this->modelName                   = $modelName;
        $this->modelClassName              = $modelClassName;
        $this->resourceName                = $resourceName;
        $this->resourceClassName           = $resourceClassName;
        $this->resourceCollectionName      = $resourceCollectionName;
        $this->resourceCollectionClassName = $resourceCollectionClassName;
        $this->arguments                   = $arguments;
    }

    public static function of(GenItemDescriptor $itemDescriptor, array $data): self {
        if (isset($data["use"])) {
            list($package, $name) = GenUtils::extractClassPackageAndName($data["use"]);
            /** @var Controller $controller */
            $controller = app($data["use"]);

            return new self($data["use"],
                $package,
                $name,
                $itemDescriptor->getModel()->getName(),
                $itemDescriptor->getModel()->getClassName(),
                $itemDescriptor->getResource()->getName(),
                $itemDescriptor->getResource()->getClassName(),
                $itemDescriptor->getResourceCollection()->getName(),
                $itemDescriptor->getResourceCollection()->getClassName(),
                $controller->getArguments());
        } else {
            $arguments = collect($data["arguments"])
                ->map(function(array $argument) { return ControllerArgument::of($argument, $argument["type"], $argument["column"]); })
                ->all();

            return new self("",
                $data["package"],
                $data["name"],
                $itemDescriptor->getModel()->getName(),
                $itemDescriptor->getModel()->getClassName(),
                $itemDescriptor->getResource()->getName(),
                $itemDescriptor->getResource()->getClassName(),
                $itemDescriptor->getResourceCollection()->getName(),
                $itemDescriptor->getResourceCollection()->getClassName(),
                $arguments);
        }
    }

    public function getName(): string {
        return $this->name;
    }

    public function generate() {
        if (!$this->use) {
            GenController::create($this->package, $this->name)
                ->setModelName($this->modelName)
                ->setModelClassName($this->modelClassName)
                ->setResourceName($this->resourceName)
                ->setResourceClassName($this->resourceClassName)
                ->setResourceCollectionName($this->resourceCollectionName)
                ->setResourceCollectionClassName($this->resourceCollectionClassName)
                ->setArguments($this->arguments)
                ->save();
        }
    }
}
