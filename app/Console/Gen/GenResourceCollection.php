<?php

namespace App\Console\Gen;

use Illuminate\Support\Str;

class GenResourceCollection {

    /** @var string */
    protected $namespace;

    /** @var string */
    protected $name;

    /** @var string */
    private $modelName;

    /** @var string */
    private $modelClassName;

    /** @var string */
    private $resourceName;

    /** @var string */
    private $resourceClassName;

    /** @var string */
    private $routeName;

    private function __construct(string $namespace, string $name) {
        $this->namespace = $namespace;
        $this->name      = $name;
    }

    /**
     * @param string $namespace
     * @param string $name
     *
     * @return self
     */
    public static function create($namespace, $name) {
        GenPreconditions::nonEmpty($namespace, "ResourceCollection namespace cannot be empty!");
        GenPreconditions::nonEmpty($name, "ResourceCollection name cannot be empty!");

        return new self($namespace, $name);
    }

    public function setModelName(string $modelName): self {
        $this->modelName = $modelName;
        return $this;
    }

    public function setModelClassName(string $modelClassName): self {
        $this->modelClassName = $modelClassName;
        return $this;
    }

    public function setResourceName(string $resourceName): self {
        $this->resourceName = $resourceName;
        return $this;
    }

    public function setResourceClassName(string $resourceClassName): self {
        $this->resourceClassName = $resourceClassName;
        return $this;
    }

    public function setRouteName(string $routeName): self {
        $this->routeName = $routeName;
        return $this;
    }

    private function renderToArray(GenClass $class) {
        $body      = "";
        $dataLine  = GenSource::start()->tab(3)->literalString("data")->literal(" => ")->variable("this")->literal("->collection")->endOfArrayLine()->end();
        $linksLine = "";

        if ($this->routeName) {
            $class->addImport("\Illuminate\Support\Facades\Auth");
            $class->addImport("\Illuminate\Contracts\Auth\Access\Authorizable");
            $class->addImport($this->modelClassName);

            $linksLine = GenSource::start()->tab(3)->literalString("links")->literal(" => ")->variable("links")->endOfArrayLine()->end();

            $body .= GenSource::start()
                ->tab(2)->startDoc()
                ->tab(2)->docVar("Authorizable", "user")
                ->tab(2)->endDoc()
                ->tab(2)->variable("user")->literal(" = Auth::user()")->endOfLine()
                ->tab(2)->variable("links")->literal(" = []")->endOfLine()
                ->blankLine()
                ->tab(2)->variable("links")->literal("[")->literalString("self")->literal("] = route(")->literalString("{$this->routeName}.findAll")->literal(")")->endOfLine()
                ->blankLine()
                ->tab(2)->literal("if (")->variable("user")->literal("->can(")->literalString("insert")->literal(", {$this->modelName}::class))")->startBrace()
                ->tab(3)->variable("links")->literal("[")->literalString("insert")->literal("] = route(")->literalString("{$this->routeName}.insert")->literal(")")->endOfLine()
                ->tab(2)->endBrace()
                ->blankLine()
                ->end();
        }

        $body .= GenSource::start()
            ->tab(2)->literal("return ")->startArray()
            ->literal($dataLine)
            ->literal($linksLine)
            ->tab(2)->endArray()
            ->end();

        $class->addMethod("public", "array", "toArray", [
            [
                "type" => "Request",
                "name" => "request",
            ],
        ], $body);
    }

    /**
     * @return string
     */
    public function render() {
        $class = GenClass::create($this->namespace, $this->name)
                         ->addImport("\Illuminate\Http\Request")
                         ->addImport("\Illuminate\Http\Resources\Json\ResourceCollection")
                         ->setExtend("ResourceCollection");

        $this->renderToArray($class);

        return $class->render();
    }

    public function save() {
        $namespace = Str::startsWith($this->namespace, "\\") ?
            $this->namespace :
            "\\{$this->namespace}";

        $pathName = GenUtils::fromClassNameToPathName("{$namespace}\\{$this->name}");

        GenFile::create($pathName)
               ->contents($this->render())
               ->save();
    }
}
