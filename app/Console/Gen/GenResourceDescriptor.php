<?php

namespace App\Console\Gen;

use App\Model\ModelColumn;

class GenResourceDescriptor {

    /** @var string */
    private $use;

    /** @var string */
    private $package;

    /** @var string */
    private $name;

    /** @var ModelColumn[] */
    private $modelColumns;

    private function __construct(string $use, string $package, string $name, array $modelColumns) {
        $this->use          = $use;
        $this->package      = $package;
        $this->name         = $name;
        $this->modelColumns = $modelColumns;
    }

    public static function of(GenItemDescriptor $itemDescriptor, array $data): self {
        if (isset($data["use"])) {
            list($package, $name) = GenUtils::extractClassPackageAndName($data["use"]);

            return new self($data["use"], $package, $name, $itemDescriptor->getModel()->getColumns());
        } else {
            return new self("", $data["package"], $data["name"], $itemDescriptor->getModel()->getColumns());
        }
    }

    public function getClassName(): string {
        return "{$this->package}\\{$this->name}";
    }

    public function getName(): string {
        return $this->name;
    }

    public function generate() {
        if (!$this->use) {
            GenResource::create($this->package, $this->name)
                ->setModelColumns($this->modelColumns)
                ->save();
        }
    }
}
