<?php

namespace App\Console\Gen;

class GenRoutesDescriptor {

    /** @var string */
    private $name;

    /** @var string */
    private $path;

    /** @var string */
    private $controllerName;

    private function __construct(string $name, string $path, string $controllerName) {
        $this->name           = $name;
        $this->path           = $path;
        $this->controllerName = $controllerName;
    }

    public static function of(GenItemDescriptor $itemDescriptor, $routesDescriptor): self {
        return new self($routesDescriptor["name"], $routesDescriptor["path"], $itemDescriptor->getController()->getName());
    }

    public function getName(): string {
        return $this->name;
    }

    public function generate() {
        GenRoutes::create("routes/{$this->name}.php")
            ->addRoute("get",    "{$this->path}",      "{$this->controllerName}@findAll", "{$this->name}.findAll")
            ->addRoute("post",   "{$this->path}",      "{$this->controllerName}@insert",  "{$this->name}.insert" )
            ->addRoute("get",    "{$this->path}/{id}", "{$this->controllerName}@findOne", "{$this->name}.findOne")
            ->addRoute("put",    "{$this->path}/{id}", "{$this->controllerName}@update",  "{$this->name}.update" )
            ->addRoute("delete", "{$this->path}/{id}", "{$this->controllerName}@delete",  "{$this->name}.delete" )
            ->save();
    }
}
