<?php

namespace App\Console\Gen;

use App\Model\Model;
use App\Model\ModelColumn;

class GenModelDescriptor {

    /** @var string */
    private $use;

    /** @var string */
    private $package;

    /** @var string */
    private $name;

    /** @var string */
    private $table;

    /** @var ModelColumn[] */
    private $columns;

    private function __construct($use, $package, $name, $table, $columns) {
        $this->use     = $use;
        $this->package = $package;
        $this->name    = $name;
        $this->table   = $table;
        $this->columns = $columns;
    }

    public static function of(GenItemDescriptor $itemDescriptor, array $data): self {
        if (isset($data["use"])) {
            list($package, $name) = GenUtils::extractClassPackageAndName($data["use"]);
            /** @var Model $model */
            $model = app($data["use"]);

            return new self($data["use"], $package, $name, $model->getTable(), $model->getColumns());
        } else {
            $columns = collect($data["columns"])
                ->map(function(array $column) { return ModelColumn::of($column, $column["type"]); })
                ->all();

            return new self("", $data["package"], $data["name"], $data["table"], $columns);
        }
    }

    public function getClassName(): string {
        return "{$this->package}\\{$this->name}";
    }

    public function getName(): string {
        return $this->name;
    }

    /**
     * @return ModelColumn[]
     */
    public function getColumns() {
        return $this->columns;
    }

    public function generate() {
        if (!$this->use) {
            GenModel::create($this->package, $this->name)
                ->setTable($this->table)
                ->setColumns($this->columns)
                ->save();
        }
    }
}
