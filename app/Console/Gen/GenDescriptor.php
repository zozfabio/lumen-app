<?php

namespace App\Console\Gen;

class GenDescriptor {

    /** @var GenItemDescriptor[] */
    private $itens;

    private function __construct(array $data) {
        $this->itens = [];

        foreach ($data as $item) {
            $this->itens[] = GenItemDescriptor::of($item);
        }
    }

    public static function of(array $data): self {
        return new self($data);
    }

    public function findResourceOfModel(string $modelClass): GenResourceDescriptor {
        foreach ($this->itens as $item) {
            $model    = $item->getModel();
            $resource = $item->getResource();
            if ($model && $resource) {
                if ($modelClass === $model->getClassName()) {
                    return $resource;
                }
            }
        }
        throw new GenClassException("Resource of model \"{$modelClass}\" is required!");
    }

    public function generate() {
        foreach ($this->itens as $item) {
            $item->generate();
        }
    }
}
