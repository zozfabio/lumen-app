<?php

namespace App\Console\Gen;

class GenResourceCollectionDescriptor {

    /** @var string */
    private $use;

    /** @var string */
    private $package;

    /** @var string */
    private $name;

    /** @var GenItemDescriptor */
    private $itemDescriptor;

    private function __construct(string $use, string $package, string $name, ?GenItemDescriptor $itemDescriptor) {
        $this->use            = $use;
        $this->package        = $package;
        $this->name           = $name;
        $this->itemDescriptor = $itemDescriptor;
    }

    public static function of(GenItemDescriptor $itemDescriptor, array $data): self {
        if (isset($data["use"])) {
            list($package, $name) = GenUtils::extractClassPackageAndName($data["use"]);

            return new self($data["use"], $package, $name, $itemDescriptor);
        } else {
            return new self("", $data["package"], $data["name"], $itemDescriptor);
        }
    }

    public function getClassName(): string {
        return "{$this->package}\\{$this->name}";
    }

    public function getName(): string {
        return $this->name;
    }

    public function generate() {
        if (!$this->use) {
            GenResourceCollection::create($this->package, $this->name)
                                 ->setModelName($this->itemDescriptor->getModel()
                                                                     ->getName())
                                 ->setModelClassName($this->itemDescriptor->getModel()
                                                                          ->getClassName())
                                 ->setResourceName($this->itemDescriptor->getResource()
                                                                        ->getName())
                                 ->setResourceClassName($this->itemDescriptor->getResource()
                                                                             ->getClassName())
                                 ->setRouteName($this->itemDescriptor->hasRoute() ? $this->itemDescriptor->getRoute()
                                                                                                         ->getName() : "")
                                 ->save();
        }
    }
}
