<?php

namespace App\Console\Gen;

class GenItemDescriptor {

    /** @var GenModelDescriptor */
    private $model;

    /** @var GenResourceDescriptor */
    private $resource;

    /** @var GenResourceCollectionDescriptor */
    private $resourceCollection;

    /** @var GenControllerDescriptor */
    private $controller;

    /** @var GenRoutesDescriptor */
    private $route;

    private function __construct(array $data) {
        if (isset($data["model"])) {
            $this->model = GenModelDescriptor::of($this, $data["model"]);

            if (isset($data["resource"])) {
                $this->resource = GenResourceDescriptor::of($this, $data["resource"]);

                if (isset($data["resourceCollection"])) {
                    $this->resourceCollection = GenResourceCollectionDescriptor::of($this, $data["resourceCollection"]);
                }

                if (isset($data["controller"])) {
                    $this->controller = GenControllerDescriptor::of($this, $data["controller"]);

                    if (isset($data["route"])) {
                        $this->route = GenRoutesDescriptor::of($this, $data["route"]);
                    }
                }
            }
        }
    }

    public static function of(array $data): self {
        return new self($data);
    }

    public function hasModel(): bool {
        return !!$this->model;
    }

    public function getModel(): ?GenModelDescriptor {
        return $this->model;
    }

    public function hasResource(): bool {
        return !!$this->resource;
    }

    public function getResource(): ?GenResourceDescriptor {
        return $this->resource;
    }

    public function hasResourceCollection(): bool {
        return !!$this->resourceCollection;
    }

    public function getResourceCollection(): ?GenResourceCollectionDescriptor {
        return $this->resourceCollection;
    }

    public function hasController(): bool {
        return !!$this->controller;
    }

    public function getController(): ?GenControllerDescriptor {
        return $this->controller;
    }

    public function hasRoute(): bool {
        return !!$this->route;
    }

    public function getRoute(): ?GenRoutesDescriptor {
        return $this->route;
    }

    public function generate() {
        if ($this->hasModel()) {
            $this->model->generate();
        }
        if ($this->hasResource()) {
            $this->resource->generate();
        }
        if ($this->hasResourceCollection()) {
            $this->resourceCollection->generate();
        }
        if ($this->hasController()) {
            $this->controller->generate();
        }
        if ($this->hasRoute()) {
            $this->route->generate();
        }
    }
}
