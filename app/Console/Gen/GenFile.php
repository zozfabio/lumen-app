<?php

namespace App\Console\Gen;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;

class GenFile {

    protected $pathName = "";

    protected $contents = "";

    /**
     * @var Filesystem
     */
    private $files;

    /**
     * @param string $pathName
     */
    private function __construct($pathName) {
        $this->pathName = $pathName;

        $this->files = app("files");
    }

    /**
     * @param string $pathName
     *
     * @return self
     */
    public static function create($pathName) {
        GenPreconditions::nonEmpty($pathName, "File path name cannot be empty!");

        return new GenFile($pathName);
    }

    /**
     * @param string $contents
     *
     * @return $this
     */
    public function contents($contents) {
        $this->contents = $contents;

        return $this;
    }

    /**
     * @return string
     */
    public function render() {
        $out  = "<?php".PHP_EOL.PHP_EOL;
        $out .= $this->contents;

        return $out;
    }

    public function save() {
        $pathName = Str::endsWith($this->pathName, ".php") ?
            $this->pathName :
            "{$this->pathName}.php";

        $this->files->put($pathName, $this->render());
    }
}
