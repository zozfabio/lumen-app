<?php

namespace App\Console\Gen;

abstract class GenPreconditions {

    public static function nonEmpty($value, $message = "Value cannot be empty!") {
        if (empty($value)) {
            throw new GenClassException($message);
        }
    }
}
