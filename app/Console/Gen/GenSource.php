<?php

namespace App\Console\Gen;

class GenSource {

    /** @var string */
    private $source;

    private function __construct(string $source) {
        $this->source = $source;
    }

    public static function start(): self {
        return new self("");
    }

    public function variable(string $name): self {
        $this->source .= '$' . $name;
        return $this;
    }

    public function literal(string $literal): self {
        $this->source .= $literal;
        return $this;
    }

    public function literalString(string $string): self {
        $this->source .= '"' . $string . '"';
        return $this;
    }

    public function tab(int $tabs = 1): self {
        $this->source .= str_repeat('    ', $tabs);
        return $this;
    }

    public function endOfLine(): self {
        $this->source .= ';' . PHP_EOL;
        return $this;
    }

    public function endOfArrayLine(): self {
        $this->source .= ',' . PHP_EOL;
        return $this;
    }

    public function startBrace(): self {
        $this->source .= ' {' . PHP_EOL;
        return $this;
    }

    public function endBrace(): self {
        $this->source .= '}' . PHP_EOL;
        return $this;
    }

    public function startArray(): self {
        $this->source .= ' [' . PHP_EOL;
        return $this;
    }

    public function endArray(): self {
        $this->source .= '];' . PHP_EOL;
        return $this;
    }

    public function startDoc(): self {
        $this->source .= '/**' . PHP_EOL;
        return $this;
    }

    public function docVar(string $type, string $name): self {
        return $this->literal(' * @var ')
            ->literal($type)
            ->literal(' ')
            ->variable($name)
            ->literal(PHP_EOL);
    }

    public function endDoc(): self {
        $this->source .= ' */' . PHP_EOL;
        return $this;
    }

    public function blankLine(): self {
        $this->source .= PHP_EOL;
        return $this;
    }

    public function end(): string {
        return $this->source;
    }
}