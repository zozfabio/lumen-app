<?php

namespace App\Console\Gen;

abstract class GenUtils {

    /**
     * @param string $pathName
     *
     * @return string
     */
    public static function fromPathNameToClassName($pathName) {
        return "\App" . str_replace("/", "\\", explode("/app", str_replace(".php", "", $pathName))[1]);
    }

    /**
     * @param string $className
     *
     * @return string
     */
    public static function fromClassNameToPathName($className) {
        return base_path("app") . str_replace("\\", "/", explode("\App", $className)[1] . ".php");
    }

    /**
     * @param string $fullClassName
     *
     * @return array|null
     */
    public static function extractClassPackageAndName($fullClassName) {
        $packs = explode("\\", $fullClassName);
        if (sizeof($packs) > 1) {
            if (empty($packs[0])) {
                array_shift($packs);
            }
            $name = array_pop($packs);
            return [
                implode("\\", $packs),
                $name,
            ];
        }
        return null;
    }
}
