<?php

namespace App\Console\Gen;

use App\Http\Controllers\ControllerArgument;
use Illuminate\Support\Str;

class GenController {

    /** @var string */
    protected $namespace;

    /** @var string */
    protected $name;

    /** @var string */
    protected $modelName;

    /** @var string */
    protected $modelClassName;

    /** @var string */
    protected $resourceName;

    /** @var string */
    protected $resourceClassName;

    /** @var string */
    protected $resourceCollectionName;

    /** @var string */
    protected $resourceCollectionClassName;

    /** @var ControllerArgument[] */
    protected $arguments;

    private function __construct(string $namespace, string $name) {
        $this->namespace = $namespace;
        $this->name      = $name;
    }

    public static function create(string $namespace, string $name) {
        GenPreconditions::nonEmpty($namespace, "Controller namespace cannot be empty!");
        GenPreconditions::nonEmpty($name,      "Controller name cannot be empty!");

        return new self($namespace, $name);
    }

    public function setModelName(string $modelName): self {
        $this->modelName = $modelName;
        return $this;
    }

    public function setModelClassName(string $modelClassName): self {
        $this->modelClassName = $modelClassName;
        return $this;
    }

    public function setResourceName(string $resourceName): self {
        $this->resourceName = $resourceName;
        return $this;
    }

    public function setResourceClassName(string $resourceClassName): self {
        $this->resourceClassName = $resourceClassName;
        return $this;
    }

    public function setResourceCollectionName(string $resourceCollectionName): self {
        $this->resourceCollectionName = $resourceCollectionName;
        return $this;
    }

    public function setResourceCollectionClassName(string $resourceCollectionClassName): self {
        $this->resourceCollectionClassName = $resourceCollectionClassName;
        return $this;
    }

    public function addArgument(ControllerArgument $argument): self {
        $this->arguments[] = $argument;
        return $this;
    }

    public function setArguments(array $arguments): self {
        $this->arguments = [];
        foreach ($arguments as $argument) {
            $this->addArgument($argument);
        }
        return $this;
    }

    private function renderModelClass(GenClass $class) {
        $value = "{$this->modelName}::class";

        $class->addImport($this->modelClassName)
            ->addProperty("protected", "string", "modelClass", $value);
    }

    private function renderResourceClass(GenClass $class) {
        $value = "{$this->resourceName}::class";

        $class->addImport($this->resourceClassName)
            ->addProperty("protected", "string", "resourceClass", $value);
    }

    private function renderResourceCollectionClass(GenClass $class) {
        $value = "{$this->resourceCollectionName}::class";

        $class->addImport($this->resourceCollectionClassName)
            ->addProperty("protected", "string", "resourceCollectionClass", $value);
    }

    private function renderArguments(GenClass $class) {
        if (sizeof($this->arguments) == 0) return;

        $class->addImport(ControllerArgument::class);

        $value = "[".PHP_EOL;
        foreach ($this->arguments as $argument) {
            list($package, $name) = GenUtils::extractClassPackageAndName($argument->getType());

            $class->addImport("{$package}\\{$name}");

            $value .= "            ControllerArgument::of(\"{$argument->getName()}\", {$name}::class, \"{$argument->getColumn()}\"),".PHP_EOL;
        }
        $value .= "        ];".PHP_EOL;

        $class->addFixedValueGetter("public", "ControllerArgument[]", "arguments", $value);
    }

    public function render(): string {
        GenPreconditions::nonEmpty($this->modelClassName, "Controller modelClass property cannot be empty!");

        $class = GenClass::create($this->namespace, $this->name)
            ->addImport("App\Http\Controllers\Controller")
            ->setExtend("Controller");

        $this->renderModelClass($class);
        $this->renderResourceClass($class);
        $this->renderResourceCollectionClass($class);
        $this->renderArguments($class);

        return $class->render();
    }

    public function save() {
        $namespace = Str::startsWith($this->namespace, "\\") ?
            $this->namespace :
            "\\{$this->namespace}";

        $pathName = GenUtils::fromClassNameToPathName("{$namespace}\\{$this->name}");

        GenFile::create($pathName)
            ->contents($this->render())
            ->save();
    }
}
