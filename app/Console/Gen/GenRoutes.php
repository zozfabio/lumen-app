<?php

namespace App\Console\Gen;

class GenRoutes {

    protected $pathName;

    protected $routes = [];

    private function __construct(string $pathName) {
        $this->pathName = $pathName;
    }

    public static function create(string $pathName): self {
        GenPreconditions::nonEmpty($pathName, "Routes path name cannot be empty!");

        return new self($pathName);
    }

    public function addRoute(string $method, string $uri, string $action, string $name): self {
        $this->routes[] = (object) [
            "method" => $method,
            "uri"    => $uri,
            "action" => $action,
            "name"   => $name,
        ];

        return $this;
    }

    private function renderRoutes(): string {
        $varSymbol = '$';

        $out = "";
        foreach ($this->routes as $route) {
            $out .= "{$varSymbol}router->{$route->method}(\"{$route->uri}\", [\"as\" => \"{$route->name}\", \"uses\" => \"{$route->action}\"]);".PHP_EOL;
        }
        return $out;
    }

    public function render(): string {
        $out  = "/**".PHP_EOL;
        $out .= ' * @var \Laravel\Lumen\Routing\Router $router'.PHP_EOL;
        $out .= ' */'.PHP_EOL.PHP_EOL;

        $out .= $this->renderRoutes();

        return $out;
    }

    public function save() {
        GenFile::create($this->pathName)
            ->contents($this->render())
            ->save();
    }
}
