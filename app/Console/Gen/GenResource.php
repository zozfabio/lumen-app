<?php

namespace App\Console\Gen;

use App\Model\ModelColumn;
use Illuminate\Support\Str;

class GenResource {

    /** @var string */
    protected $namespace = "";

    /** @var string */
    protected $name = "";

    /** @var ModelColumn[] */
    protected $modelColumns = [];

    private function __construct($namespace, $name) {
        $this->namespace = $namespace;
        $this->name      = $name;
    }

    public static function create(string $namespace, string $name): self {
        GenPreconditions::nonEmpty($namespace, "Resource namespace cannot be empty!");
        GenPreconditions::nonEmpty($name,      "Resource name cannot be empty!");

        return new self($namespace, $name);
    }

    public function addModelColumn(ModelColumn $modelColumn): self {
        $this->modelColumns[] = $modelColumn;
        return $this;
    }

    public function setModelColumns(array $modelColumns): self {
        $this->modelColumns = [];
        foreach ($modelColumns as $modelColumn) {
            $this->addModelColumn($modelColumn);
        }
        return $this;
    }

    private function renderToArray(GenClass $class) {
        /** @var GenDescriptor $descriptor */
        $descriptor = app(GenDescriptor::class);
        $propSymbol = '$';

        $body = "        return [".PHP_EOL;
        $body.= "            \"data\" => [".PHP_EOL;
        foreach ($this->modelColumns as $modelColumn) {
            switch ($modelColumn->getType()) {
                case ModelColumn::TYPE_INT:
                case ModelColumn::TYPE_STRING:
                    $body.= "                \"{$modelColumn->getName()}\" => {$propSymbol}this->resource->{$modelColumn->getName()},".PHP_EOL;
                    break;
            }
        }
        $body.= PHP_EOL;
        foreach ($this->modelColumns as $modelColumn) {
            switch ($modelColumn->getType()) {
                case ModelColumn::TYPE_MANY_TO_ONE:
                    $resourceDescriptor = $descriptor->findResourceOfModel($modelColumn->getRelated());

                    $class->addImport($resourceDescriptor->getClassName());

                    $body.= "                \"{$modelColumn->getName()}\" => new {$resourceDescriptor->getName()}({$propSymbol}this->whenLoaded(\"{$modelColumn->getName()}\")),".PHP_EOL;
                    break;
            }
        }
        $body.= "            ],".PHP_EOL;
        $body.= "        ];".PHP_EOL;

        $class->addMethod("public", "array", "toArray", [["type" => "Request", "name" => "request"]], $body);
    }

    public function render(): string {
        $class = GenClass::create($this->namespace, $this->name)
            ->addImport("\Illuminate\Http\Request")
            ->addImport("\Illuminate\Http\Resources\Json\Resource")
            ->setExtend("Resource");

        $this->renderToArray($class);

        return $class->render();
    }

    public function save() {
        $namespace = Str::startsWith($this->namespace, "\\") ?
            $this->namespace :
            "\\{$this->namespace}";

        $pathName = GenUtils::fromClassNameToPathName("{$namespace}\\{$this->name}");

        GenFile::create($pathName)
            ->contents($this->render())
            ->save();
    }
}
