<?php

namespace App\Console\Gen;

use Illuminate\Support\Str;

class GenClass {

    protected $namespace = "";

    protected $name = "";

    protected $parent = "";

    protected $imports = [];
    protected $constants = [];
    protected $properties = [];
    protected $methods = [];

    private function __construct(string $namespace, string $name) {
        $this->namespace = $namespace;
        $this->name      = $name;
    }

    public static function create(string $namespace, string $name): self {
        GenPreconditions::nonEmpty($namespace, "Class namespace cannot be empty!");
        GenPreconditions::nonEmpty($name,      "Class name cannot be empty!");

        return new GenClass($namespace, $name);
    }

    public function setExtend(string $parent): self {
        $this->parent = $parent;

        return $this;
    }

    public function addImport(string $className): self {
        if (!array_key_exists($className, $this->imports)) {
            $this->imports[$className] = (object) [
                "className" => $className
            ];
        }

        return $this;
    }

    public function addProperty(string $encapsulation, string $type, string $name, string $defaultValue = ""): self {
        $this->properties[] = (object) [
            "encapsulation" => $encapsulation,
            "type"          => $type,
            "name"          => $name,
            "defaultValue"  => $defaultValue,
        ];

        return $this;
    }

    public function addConstant(string $name, string $value = ""): self {
        $this->constants[] = (object) [
            "name"  => $name,
            "value" => $value,
        ];

        return $this;
    }

    public function addMethod(string $encapsulation, string $returnType, string $name, array $parameters, string $body) {
        $this->methods[] = (object) [
            "encapsulation" => $encapsulation,
            "returnType"    => $returnType,
            "name"          => $name,
            "parameters"    => collect($parameters)->map(function(array $parameter) { return (object) [
                "type" => $parameter["type"],
                "name" => $parameter["name"],
            ]; })->all(),
            "body"          => $body,
        ];

        return $this;
    }

    public function addFixedValueGetter(string $encapsulation, string $returnType, string $name, string $value = ""): self {
        $name = ucfirst($name);

        $this->addMethod($encapsulation, $returnType, "get{$name}", [], "        return {$value}");

        return $this;
    }

    private function renderImports(): string {
        $out = PHP_EOL;
        foreach ($this->imports as $import) {
            $className = Str::startsWith($import->className, "\\") ?
                Str::replaceFirst("\\", "", $import->className) :
                $import->className;

            $out .= PHP_EOL."use {$className};";
        }
        $out .= sizeof($this->imports) > 0 ? PHP_EOL : "";
        $out .= PHP_EOL;
        return $out;
    }

    private function renderConstants(): string {
        if (sizeof($this->constants) == 0) return "";

        $out = "";
        foreach ($this->constants as $constant) {
            $out .= PHP_EOL;
            $out .= "    const {$constant->name} = {$constant->value};".PHP_EOL;
        }
        return $out;
    }

    private function renderProperties(): string {
        if (sizeof($this->properties) == 0) return "";

        $propSymbol = '$';

        $out = "";
        foreach ($this->properties as $property) {
            $out .= PHP_EOL;
            $out .= "    /**".PHP_EOL;
            $out .= "     * @var {$property->type}".PHP_EOL;
            $out .= "     */".PHP_EOL;
            $out .= "    {$property->encapsulation} {$propSymbol}{$property->name}";
            $out .= (!empty($property->defaultValue) ? " = {$property->defaultValue};" : ";").PHP_EOL;
        }
        return $out;
    }

    private function renderMethods(): string {
        if (sizeof($this->methods) == 0) return "";

        $propSymbol = '$';

        $out = "";
        foreach ($this->methods as $method) {
            $docParams = "";
            $params    = [];
            foreach ($method->parameters as $parameter) {
                $docParams.= "     * @param {$parameter->type} {$propSymbol}{$parameter->name}".PHP_EOL;
                $params[]  = "{$propSymbol}{$parameter->name}";
            }
            $params = implode(", ", $params);

            $docs = "    /**".PHP_EOL;
            $docs.=         $docParams;
            $docs.= "     * @return {$method->returnType}".PHP_EOL;
            $docs.= "     */".PHP_EOL;

            $out .= PHP_EOL;
            $out .= $docs;
            $out .= "    {$method->encapsulation} function {$method->name}($params) {".PHP_EOL;
            $out .= "{$method->body}";
            $out .= "    }".PHP_EOL;
        }
        return $out;
    }

    public function render(): string {
        $namespace = Str::startsWith($this->namespace, "\\") ?
            Str::replaceFirst("\\", "", $this->namespace) :
            $this->namespace;

        $out  = "namespace {$namespace};";

        $out .= $this->renderImports();

        $out .= "class {$this->name} ".(!empty($this->parent) ? "extends {$this->parent} {" : "{").PHP_EOL;
        $out .= $this->renderConstants();
        $out .= $this->renderProperties();
        $out .= $this->renderMethods();
        $out .= "}".PHP_EOL;

        return $out;
    }

    public function save() {
        $namespace = Str::startsWith($this->namespace, "\\") ?
            $this->namespace :
            "\\{$this->namespace}";

        $pathName = GenUtils::fromClassNameToPathName("{$namespace}\\{$this->name}");

        GenFile::create($pathName)
            ->contents($this->render())
            ->save();
    }
}
