<?php

namespace App\Console\Commands;

use App\Console\Gen\GenDescriptor;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

class GenScaffoldingCommand extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gen:scaffolding';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate API from Models.';

    /**
     * @var Filesystem
     */
    private $files;

    /**
     * @param Filesystem $files
     */
    public function __construct(Filesystem $files) {
        parent::__construct();
        $this->files = $files;
    }

    /**
     * Execute the console command.
     */
    public function handle() {
        /** @var GenDescriptor $descriptor */
        $descriptor = app(GenDescriptor::class);

        $descriptor->generate();
    }
}
