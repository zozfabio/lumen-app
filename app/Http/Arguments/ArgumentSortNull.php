<?php

namespace App\Http\Arguments;

use Illuminate\Database\Eloquent\Builder;

class ArgumentSortNull extends ArgumentSort {

    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    public function append($builder) {
        return $builder;
    }
}
