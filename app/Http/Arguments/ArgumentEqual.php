<?php

namespace App\Http\Arguments;

abstract class ArgumentEqual implements Argument {

    /**
     * @param string $name
     * @param mixed $value
     *
     * @return ArgumentEqual
     */
    public static function of($name, $value) {
        if (!is_null($value) and $value !== "") {
            if (is_array($value)) {
                return new ArgumentEqualArray($name, $value);
            } else {
                return new ArgumentEqualDefault($name, $value);
            }
        }
        return new ArgumentEqualNull();
    }
}
