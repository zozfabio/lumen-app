<?php

namespace App\Http\Arguments;

use Illuminate\Database\Eloquent\Builder;

class ArgumentEqualArray extends ArgumentEqual {

    /**
     * @var string
     */
    private $name;

    /**
     * @var array
     */
    private $value;

    /**
     * @param string $name
     * @param array $value
     */
    public function __construct($name, array $value) {
        $this->value = $value;
        $this->name = $name;
    }

    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    public function append($builder) {
        return $builder->whereIn($this->name, $this->value);
    }
}
