<?php

namespace App\Http\Arguments;

use Illuminate\Database\Eloquent\Builder;

class ArgumentEqualDefault extends ArgumentEqual {

    /**
     * @var string
     */
    private $name;

    /**
     * @var mixed
     */
    private $value;

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __construct($name, $value) {
        $this->name = $name;
        $this->value = $value;
    }

    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    public function append($builder) {
        return $builder->where($this->name, $this->value);
    }
}
