<?php

namespace App\Http\Arguments;

use Illuminate\Database\Eloquent\Builder;

interface Argument {

    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    public function append($builder);
}
