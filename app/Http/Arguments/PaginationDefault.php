<?php

namespace App\Http\Arguments;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;

class PaginationDefault extends Pagination {

    /**
     * @var int
     */
    private $page;

    /**
     * @var int
     */
    private $size;

    /**
     * @param int $page
     * @param int $size
     */
    public function __construct($page, $size) {
        $this->page = $page;
        $this->size = $size;
    }

    /**
     * @param Builder $builder
     *
     * @return Paginator
     */
    public function paginate(Builder $builder) {
        return $builder->paginate($this->size, [], "page", $this->page);
    }
}
