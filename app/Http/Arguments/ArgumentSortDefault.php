<?php

namespace App\Http\Arguments;

use Illuminate\Database\Eloquent\Builder;

class ArgumentSortDefault extends ArgumentSort {

    /**
     * @var int
     */
    private $column;

    /**
     * @var int
     */
    private $direction;

    /**
     * @param int $column
     * @param int $direction
     */
    public function __construct($column, $direction) {
        $this->column = $column;
        $this->direction = $direction;
    }

    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    public function append($builder) {
        return $builder->orderBy($this->column, $this->direction);
    }
}
