<?php

namespace App\Http\Arguments;

abstract class ArgumentContains implements Argument {

    /**
     * @param string $name
     * @param mixed $value
     *
     * @return ArgumentContains
     */
    public static function of($name, $value) {
        if (!is_null($value) and $value !== "") {
            return new ArgumentContainsDefault($name, $value);
        }
        return new ArgumentContainsNull();
    }
}
