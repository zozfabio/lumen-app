<?php

namespace App\Http\Arguments;

use Illuminate\Database\Eloquent\Builder;

class ArgumentContainsNull extends ArgumentContains {
    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    public function append($builder) {
        return $builder;
    }
}
