<?php

namespace App\Http\Arguments;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

abstract class Pagination {

    /**
     * @param Builder $builder
     *
     * @return Paginator|Collection
     */
    public abstract function paginate(Builder $builder);

    /**
     * @param array $input
     *
     * @return Pagination
     */
    public static function of($input) {
        if (isset($input["page"])) {
            return new PaginationDefault($input["page"], $input["size"] ?? 10);
        }
        return new PaginationNull();
    }
}
