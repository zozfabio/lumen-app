<?php

namespace App\Http\Arguments;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class PaginationNull extends Pagination {

    /**
     * @param Builder $builder
     *
     * @return Collection
     */
    public function paginate(Builder $builder) {
        return $builder->get();
    }
}
