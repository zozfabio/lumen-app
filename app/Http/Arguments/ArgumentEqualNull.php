<?php

namespace App\Http\Arguments;

use Illuminate\Database\Eloquent\Builder;

class ArgumentEqualNull extends ArgumentEqual {
    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    public function append($builder) {
        return $builder;
    }
}
