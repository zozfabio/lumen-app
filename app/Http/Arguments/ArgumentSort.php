<?php

namespace App\Http\Arguments;

abstract class ArgumentSort implements Argument {

    /**
     * @param string $value
     *
     * @return ArgumentSort
     */
    private static function ofOrder($value) {
        $order = explode(',', $value);
        if (sizeof($order) === 2) {
            return new ArgumentSortDefault($order[0], $order[1]);
        } elseif (sizeof($order) === 1) {
            return new ArgumentSortDefault($order[0], "asc");
        }
        return new ArgumentSortNull();
    }

    /**
     * @param array $input
     *
     * @return ArgumentSort[]
     */
    public static function of($input) {
        $sort = [];

        if (isset($input["sort"])) {
            $value = $input["sort"];
            if (!is_null($value) and $value !== "") {
                if (is_array($value)) {
                    foreach ($value as $itemValue) {
                        $sort[] = self::ofOrder($itemValue);
                    }
                } else {
                    $sort[] = self::ofOrder($value);
                }
            }
        }

        return $sort;
    }
}
