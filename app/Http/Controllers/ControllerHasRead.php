<?php

namespace App\Http\Controllers;

use App\Http\Arguments\Pagination;
use App\Model\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * @method Builder getQueryFromRequest(array $input)
 * @method array getAllPathParameters(Request $request)
 *
 * @method Resource newResourceInstance(Model $model)
 * @method newResourceCollectionInstance(Collection $collection): ResourceCollection
 */
trait ControllerHasRead {

    /**
     * @param Request $request
     *
     * @return ResourceCollection
     */
    public function findAll(Request $request) {
        $input  = $request->input();

        $query  = $this->getQueryFromRequest($input);
        $result = Pagination::of($input)->paginate($query);

        return $this->newResourceCollectionInstance($result);
    }

    /**
     * @param Request $request
     *
     * @return Resource
     */
    public function findOne(Request $request) {
        $params = $this->getAllPathParameters($request);

        $query  = $this->getQueryFromRequest($params);
        $result = $query->firstOrFail();

        return new Resource($result);
    }
}
