<?php

namespace App\Http\Controllers;

use App\Http\Arguments\Argument;

class ControllerArgument {

    private $name = "";

    private $type = "";

    private $column = "";

    private function __construct(string $name, string $type, string $column) {
        $this->name = $name;
        $this->type = $type;
        $this->column = $column;
    }

    /**
     * @param mixed  $name
     * @param string $type
     * @param string $column
     *
     * @return self
     */
    public static function of($name, $type, $column) {
        if (is_array($name)) {
            $type   = $name["type"];
            $column = $name["column"];
            $name   = $name["name"];
        }
        return new self($name, $type, $column);
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getType(): string {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getColumn(): string {
        return $this->column;
    }

    /**
     * @param mixed $value
     *
     * @return Argument
     */
    public function newArgumentInstance($value): Argument {
        return call_user_func([$this->type, "of"], $this->column, $value);
    }
}
