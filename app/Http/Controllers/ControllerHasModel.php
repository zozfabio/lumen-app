<?php

namespace App\Http\Controllers;

use App\Model\Model;
use Illuminate\Database\Eloquent\Builder;

trait ControllerHasModel {

    private $modelInstance = null;

    protected $modelClass   = "";
    protected $modelFactory = "";

    protected function getModelInstance(): Model {
        if (!$this->modelInstance) {
            $this->modelInstance = app($this->modelClass);
        }
        return $this->modelInstance;
    }

    protected function newModelInstance($attributes = []): Model {
        if ($this->modelFactory) {
            return call_user_func([$this->modelClass, $this->modelFactory], $attributes);
        }
        return new $this->modelClass($attributes);
    }

    protected function getQuery(): Builder {
        $model = $this->getModelInstance();
        $query = $model->newQuery();

        return $query;
    }
}
