<?php

namespace App\Http\Controllers;

use App\Model\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Pagination\LengthAwarePaginator;

trait ControllerHasResources {

    protected $resourceClass = Resource::class;
    protected $resourceFactory = "";

    protected $resourceCollectionClass = ResourceCollection::class;
    protected $resourceCollectionFactory = "";

    protected function newResourceInstance(Model $model): Resource {
        if ($this->resourceFactory) {
            return call_user_func([$this->resourceClass, $this->resourceFactory], $model);
        }
        return new $this->resourceClass($model);
    }

    /**
     * @param Collection|LengthAwarePaginator $collection
     *
     * @return ResourceCollection
     */
    protected function newResourceCollectionInstance($collection): ResourceCollection {
        if ($this->resourceCollectionFactory) {
            return call_user_func([$this->resourceCollectionClass, $this->resourceCollectionFactory], $collection);
        }
        return new $this->resourceCollectionClass($collection);
    }
}
