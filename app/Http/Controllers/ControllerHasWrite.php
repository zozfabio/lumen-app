<?php

namespace App\Http\Controllers;

use App\Model\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @method Builder getQueryFromRequest(array $input)
 * @method array getAllPathParameters(Request $request)
 * @method Model newModelInstance(array $attributes = [])
 */
trait ControllerHasWrite {

    /**
     * @param Request $request
     *
     * @return Resource
     * @throws \Throwable
     */
    public function insert(Request $request) {
        $input = $request->input();
        $model = $this->newModelInstance($input);

        $model->saveOrFail();

        return new Resource($model);
    }

    /**
     * @param Request $request
     *
     * @return Resource
     * @throws \Throwable
     */
    public function update(Request $request) {
        $params = $this->getAllPathParameters($request);
        $input  = $request->input();
        $query  = $this->getQueryFromRequest($params);
        $model  = $query->firstOrFail();

        $model->fill($input);
        $model->saveOrFail();

        return new Resource($model);
    }

    /**
     * @param Request $request
     *
     * @throws \Throwable
     */
    public function delete(Request $request) {
        $params = $this->getAllPathParameters($request);
        $query  = $this->getQueryFromRequest($params);

        $query->delete();
    }
}
