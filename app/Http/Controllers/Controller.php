<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

abstract class Controller extends BaseController {

    use ControllerHasModel,
        ControllerHasResources,
        ControllerHasArguments,

        ControllerHasRead,
        ControllerHasWrite;

    public function __construct() {
        $this->arguments = $this->getArguments();
    }

    protected function getAllPathParameters(Request $request): array {
        list(,,$params) = $request->getRouteResolver()();

        return $params;
    }
}
