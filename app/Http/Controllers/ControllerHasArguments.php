<?php

namespace App\Http\Controllers;

use App\Http\Arguments\Argument;
use App\Http\Arguments\ArgumentSort;
use Illuminate\Database\Eloquent\Builder;

/**
 * @method getQuery(): Builder
 */
trait ControllerHasArguments {

    /**
     * @var ControllerArgument[]
     */
    protected $arguments;

    /**
     * @return ControllerArgument[]
     */
    public abstract function getArguments();

    protected function newArgumentInstance(string $argumentClass, string $field, $value): Argument {
        return call_user_func([$argumentClass, "of"], $field, $value);
    }

    /**
     * @return Argument[]
     */
    private function getArgumentsFromRequest(array $input): array {
        $arguments = collect();

        foreach ($this->arguments as $argument) {
            if (isset($input[$argument->getName()])) {
                $arguments->push($argument->newArgumentInstance($input[$argument->getName()]));
            }
        }

        $arguments->concat(ArgumentSort::of($input));

        return $arguments->all();
    }

    protected function getQueryFromRequest(array $input): Builder {
        $query = $this->getQuery();

        foreach ($this->getArgumentsFromRequest($input) as $argument) {
            $argument->append($query);
        }

        return $query;
    }
}
