<?php

namespace App\Http\Controllers;

use App\Model\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Controller;

class AuthenticationController extends Controller {

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function authenticate(Request $request): JsonResponse {
        $this->validate($request, [
            'email'    => 'required',
            'password' => 'required',
        ]);
        /** @var User $user */
        $user = User::where('email', $request->input('email'))
                    ->first();
        $password = $request->input('password');
        if (Hash::check($password, $user->password)) {
            $apiToken = base64_encode(str_random(40));
            User::where('email', $request->input('email'))
                ->update(['api_token' => "$apiToken"]);
            return response()->json([
                'status'    => 'success',
                'api_token' => $apiToken,
            ]);
        } else {
            return response()->json([
                'status' => 'fail',
            ], 401);
        }
    }
}
