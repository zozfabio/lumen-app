<?php

namespace App\Providers;

use App\Console\Gen\GenDescriptor;
use Illuminate\Support\ServiceProvider;

class GenServiceProvider extends ServiceProvider {

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        $this->app->singleton(GenDescriptor::class, function() {
            $data = config("gen");

            return GenDescriptor::of($data);
        });
    }
}
