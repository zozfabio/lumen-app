<?php

namespace App\Providers;

use App\Model\User;
use App\Policies\UserPolicy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider {

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot() {
        Gate::policy(User::class, UserPolicy::class);

        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.
        $this->app['auth']->viaRequest('api', function (Request $request): User {
            if ($request->header('Authorization')) {
                $key  = explode(' ', $request->header('Authorization'));
                $user = User::where('api_token', $key[1])
                             ->first();
                return $user;
            }
            return null;
        });
    }
}
