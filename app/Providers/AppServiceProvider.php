<?php

namespace App\Providers;

use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        Log::debug("Start listening for queries...");
        if (App::environment("local")) {
            DB::listen(function(QueryExecuted $query) {
                Log::debug("[$query->time] $query->sql", $query->bindings);
            });
        }
    }
}
