<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model as BaseModel;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

abstract class Model extends BaseModel {

    public $timestamps = false;

    protected $columns;

    public function __construct(array $attributes = []) {
        $this->columns = $this->getColumns();

        $this->primaryKey = collect($this->columns)
            ->filter(function(ModelColumn $column) { return $column->isPrimaryKey(); })
            ->map(function(ModelColumn $column) { return $column->getDbName(); })
            ->first();

        $this->fillable = collect($this->columns)
            ->filter(function(ModelColumn $column) { return $column->isFillable(); })
            ->map(function(ModelColumn $column) { return $column->getDbName(); })
            ->filter(function($column) { return !empty($column); })
            ->all();

        $this->hidden = collect($this->columns)
            ->filter(function(ModelColumn $column) { return $column->isHidden(); })
            ->map(function(ModelColumn $column) { return $column->getName(); })
            ->all();

        $this->with = collect($this->columns)
            ->filter(function(ModelColumn $column) { return $column->isRelationAutoLoadable(); })
            ->map(function(ModelColumn $column) { return $column->getName(); })
            ->all();

        parent::__construct($attributes);
    }

    /**
     * @return ModelColumn[]
     */
    public abstract function getColumns();

    /**
     * @return string[]
     */
    public function getColumnNames() {
        return collect($this->columns)
            ->map(function(ModelColumn $column) { return $column->getDbName(); })
            ->filter(function($column) { return !empty($column); })
            ->all();
    }

    public function newQuery(): Builder {
        $query = parent::newQuery();

        foreach ($this->getColumnNames() as $column) {
            $query->addSelect($column);
        }

        return $query;
    }

    public function createSchema() {
        Schema::create($this->table, function(Blueprint $table) {
            foreach ($this->columns as $column) {
                $column->createSchema($table);
            }
        });
    }

    public function createForeignKeys() {
        Schema::table($this->table, function(Blueprint $table) {
            foreach ($this->columns as $column) {
                $column->createForeignKey($table);
            }
        });
    }

    public function destroySchema() {
        Schema::dropIfExists($this->table);
    }

    public function destroyForeignKeys() {
        Schema::table($this->table, function(Blueprint $table) {
            foreach ($this->columns as $column) {
                $column->dropForeignKey($table);
            }
        });
    }

    /**
     * Handle dynamic method calls into the model.
     *
     * @param  string  $method
     * @param  array   $parameters
     * @return mixed
     */
    public function __call($method, $parameters) {
        $relationColumns = collect($this->columns)
            ->filter(function(ModelColumn $column) { return $column->isRelation(); })
            ->filter(function(ModelColumn $column) use ($method) { return $method == $column->getName(); });

        if ($relationColumns->count() == 1) {
            /** @var ModelColumn $column */
            $column = $relationColumns->first();
            return $column->createRelation($this);
        }

        return parent::__call($method, $parameters);
    }
}
