<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Schema\Blueprint;

class ModelColumn {

    const TYPE_INT          = "INT";
    const TYPE_STRING       = "STRING";
    const TYPE_MANY_TO_ONE  = "MANY_TO_ONE";
    const TYPE_ONE_TO_MANY  = "ONE_TO_MANY";
    const TYPE_MANY_TO_MANY = "MANY_TO_MANY";

    private $name;

    private $type;

    private $size;

    private $nullable;

    private $primaryKey;

    private $fillable;

    private $hidden;

    private $related;

    private $relatedInstance = null;

    private $relation = null;

    /**
     * @param string $name
     * @param string $type
     * @param int    $size
     * @param bool   $nullable
     * @param bool   $primaryKey
     * @param bool   $fillable
     * @param bool   $hidden
     * @param string $related
     */
    private function __construct($name, $type, $size, $nullable, $primaryKey, $fillable, $hidden, $related = "") {
        $this->name       = $name;
        $this->type       = $type;
        $this->nullable   = $nullable;
        $this->size       = $size;
        $this->primaryKey = $primaryKey;
        $this->fillable   = $fillable;
        $this->hidden     = $hidden;
        $this->related    = $related;
    }

    /**
     * @param mixed  $name
     * @param string $type
     * @param int    $size
     * @param bool   $nullable
     * @param bool   $primaryKey
     * @param bool   $fillable
     * @param bool   $hidden
     * @param string $related
     *
     * @return self
     */
    public static function of($name, $type, $size = 0, $nullable = false, $primaryKey = false, $fillable = true, $hidden = false, $related = "") {
        if (is_array($name)) {
            $type       = $name["type"];
            $size       = isset($name["size"])       ? (int)  $name["size"]       : 0;
            $nullable   = isset($name["nullable"])   ? (bool) $name["nullable"]   : false;
            $primaryKey = isset($name["primaryKey"]) ? (bool) $name["primaryKey"] : false;
            $fillable   = isset($name["fillable"])   ? (bool) $name["fillable"]   : true;
            $hidden     = isset($name["hidden"])     ? (bool) $name["hidden"]     : false;
            $related    = isset($name["related"])    ?        $name["related"]    : "";
            $name       = $name["name"];
        }
        return new self($name, $type, $size, $nullable, $primaryKey, $fillable, $hidden, $related);
    }

    public function getName(): string {
        return $this->name;
    }

    public function getDbName(): string {
        if ($this->isRelation()) {
            if ($this->type == self::TYPE_MANY_TO_ONE) {
                return "{$this->name}_id";
            }
            return "";
        }
        return $this->name;
    }

    public function getType(): string {
        return $this->type;
    }

    public function getSize(): int {
        return $this->size;
    }

    public function setSize(int $size): ModelColumn {
        $this->size = $size;
        return $this;
    }

    public function isNullable(): bool {
        return $this->nullable;
    }

    public function setNullable(bool $nullable): ModelColumn {
        $this->nullable = $nullable;
        return $this;
    }

    public function isPrimaryKey(): bool {
        return $this->primaryKey;
    }

    public function setPrimaryKey(bool $primaryKey): ModelColumn {
        $this->primaryKey = $primaryKey;
        return $this;
    }

    public function isFillable(): bool {
        return $this->fillable;
    }

    public function setFillable(bool $fillable): ModelColumn {
        $this->fillable = $fillable;
        return $this;
    }

    public function isHidden(): bool {
        return $this->hidden;
    }

    public function setHidden(bool $hidden): ModelColumn {
        $this->hidden = $hidden;
        return $this;
    }

    public function getRelated(): string {
        return $this->related;
    }

    public function setRelated(string $related): ModelColumn {
        $this->related = $related;
        return $this;
    }

    public function isRelation(): bool {
        return !empty($this->related);
    }

    public function isRelationAutoLoadable(): bool {
        return $this->isRelation() && $this->type == self::TYPE_MANY_TO_ONE;
    }

    private function getRelatedInstance(): Model {
        if ($this->relatedInstance) {
            return $this->relatedInstance;
        }
        return $this->relatedInstance = app($this->related);
    }

    public function createRelation(Model $model): Relation {
        if ($this->isRelation() && is_null($this->relation)) {
            switch ($this->type) {
                case self::TYPE_MANY_TO_ONE:
                    $this->relation = $model->belongsTo($this->related, "{$this->name}_id", "id", $this->name);
                    break;
                case self::TYPE_ONE_TO_MANY:
                    $modelName      = snake_case(class_basename($model));
                    $this->relation = $model->hasMany($this->related, "{$modelName}_id", "id");
                    break;
                case self::TYPE_MANY_TO_MANY:
                    $modelName      = snake_case(class_basename($model));
                    $relationName   = snake_case(class_basename($this->related));
                    $this->relation = $model->belongsToMany($this->related, "{$modelName}_{$relationName}", "{$modelName}_id", "{$relationName}_id", "{$modelName}_id", "{$relationName}_id", $this->name);
                    break;
            }
        }
        return $this->relation;
    }

    public function createSchema(Blueprint $table) {
        switch ($this->type) {
            case self::TYPE_INT:
                if ($this->primaryKey) {
                    $table->integer($this->getDbName())
                        ->autoIncrement();
                } else {
                    $table->integer($this->getDbName())
                        ->nullable($this->isNullable());
                }
                break;
            case self::TYPE_STRING:
                $table->string($this->getDbName(), $this->getSize())
                    ->nullable($this->isNullable());
                break;
            case self::TYPE_MANY_TO_ONE:
                $table->integer($this->getDbName())
                    ->nullable($this->isNullable());
                break;
        }
    }

    public function createForeignKey(Blueprint $table) {
        switch ($this->type) {
            case self::TYPE_MANY_TO_ONE:
                $table->foreign($this->getDbName(), "{$table->getTable()}_{$this->getName()}_fk")
                    ->references("id")
                    ->on($this->getRelatedInstance()->getTable());
                break;
        }
    }

    public function dropForeignKey(Blueprint $table) {
        switch ($this->type) {
            case self::TYPE_MANY_TO_ONE:
                $table->dropForeign("{$table->getTable()}_{$this->getName()}_fk");
                break;
        }
    }

    public static function nonHiddenFilter() {
        return function(ModelColumn $column) {
            return !$column->isHidden();
        };
    }
}
