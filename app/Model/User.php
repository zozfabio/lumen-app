<?php

namespace App\Model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Auth\Authorizable;

/**
 * Users Model
 *
 * @property int    $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $api_token
 *
 * @property \App\Model\Country $country
 * @property \App\Model\State   $state
 * @property \App\Model\City    $city
 * @property string  $zip_code
 * @property string  $street
 *
 * @property Collection|\App\Model\UserContact[] $contacts
 *
 * @method BelongsTo country()
 * @method BelongsTo state()
 * @method BelongsTo city()
 * @method HasMany   contacts()
 */
class User extends Model implements AuthenticatableContract, AuthorizableContract {

    use Authenticatable, Authorizable;

    const TABLE = "user";

    protected $table = self::TABLE;

    public function getColumns() {
        return [
            ModelColumn::of("id", ModelColumn::TYPE_INT)
                ->setPrimaryKey(true)
                ->setFillable(false),
            ModelColumn::of("name", ModelColumn::TYPE_STRING)
                ->setSize(100),
            ModelColumn::of("email", ModelColumn::TYPE_STRING)
                ->setSize(100),
            ModelColumn::of("password", ModelColumn::TYPE_STRING)
                ->setSize(100)
                ->setHidden(true),
            ModelColumn::of("api_token", ModelColumn::TYPE_STRING)
                ->setSize(100)
                ->setNullable(true)
                ->setHidden(true),
            ModelColumn::of("country", ModelColumn::TYPE_MANY_TO_ONE)
                ->setHidden(true)
                ->setNullable(true)
                ->setRelated("\App\Model\Country"),
            ModelColumn::of("state", ModelColumn::TYPE_MANY_TO_ONE)
                ->setNullable(true)
                ->setRelated("\App\Model\State"),
            ModelColumn::of("city", ModelColumn::TYPE_MANY_TO_ONE)
                ->setNullable(true)
                ->setRelated("\App\Model\City"),
            ModelColumn::of("zip_code", ModelColumn::TYPE_STRING)
                ->setNullable(true)
                ->setSize(100),
            ModelColumn::of("street", ModelColumn::TYPE_STRING)
                ->setNullable(true)
                ->setSize(100),
            ModelColumn::of("contacts", ModelColumn::TYPE_ONE_TO_MANY)
                ->setRelated("\App\Model\UserContact"),
        ];
    }

    public function setPasswordAttribute($password) {
        $this->attributes["password"] = Hash::make($password);
    }
}
