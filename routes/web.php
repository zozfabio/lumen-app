<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/**
 * @var \Illuminate\Filesystem\Filesystem $files
 * @var \Laravel\Lumen\Routing\Router $router
 */

$files = app("files");

$router->post('/authenticate', "AuthenticationController@authenticate");

$router->group(["middleware" => "auth"], function () use ($files, $router) {
    foreach ($files->allFiles(base_path("routes")) as $file) {
        if ($file->getFilename() !== "web.php") {
            require_once $file->getPathname();
        }
    }
});

