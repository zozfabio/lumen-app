<?php

use App\Model\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        User::create(["name" => "Admin", "email" => "admin@lumenapp.com", "password" => "123123"]);
    }
}
