<?php

use Illuminate\Database\Migrations\Migration;

class CreateTables extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        app(\App\Model\Country::class)->createSchema();
        app(\App\Model\State::class)->createSchema();
        app(\App\Model\City::class)->createSchema();
        app(\App\Model\User::class)->createSchema();
        app(\App\Model\UserContact::class)->createSchema();

        app(\App\Model\Country::class)->createForeignKeys();
        app(\App\Model\State::class)->createForeignKeys();
        app(\App\Model\City::class)->createForeignKeys();
        app(\App\Model\User::class)->createForeignKeys();
        app(\App\Model\UserContact::class)->createForeignKeys();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        app(\App\Model\Country::class)->destroyForeignKeys();
        app(\App\Model\State::class)->destroyForeignKeys();
        app(\App\Model\City::class)->destroyForeignKeys();
        app(\App\Model\User::class)->destroyForeignKeys();
        app(\App\Model\UserContact::class)->destroyForeignKeys();

        app(\App\Model\Country::class)->destroySchema();
        app(\App\Model\State::class)->destroySchema();
        app(\App\Model\City::class)->destroySchema();
        app(\App\Model\User::class)->destroySchema();
        app(\App\Model\UserContact::class)->destroySchema();
    }
}
